#include "pch-il2cpp.h"

#include "helpers.h"

#include "../user/CheatMain.h"
#include "../user/utils.h"

using namespace app;

app::Type* CheatMain::ColliderType = NULL;

CheatMain::CheatMain()
{
	oRaycastHit_GetCollider = (dRaycastHit_GetCollider)(il2cppi_get_base_address() + 0x1326090);
	if (((std::uintptr_t)oRaycastHit_GetCollider == 0) || ((std::uintptr_t)oRaycastHit_GetCollider == 0x1326090))
		il2cppi_log_write("Could not get RaycastHit_GetCollider!");

	Entities = std::vector<ESPTarget>();
	NumEntitiesCached = 0;

	LocalPlayerTrans = NULL;
	MainCamTrans = NULL;
}

void CheatMain::PerFrameUpdate(bool shouldDoAimbot)
{
	// TO-DO:
	// make sure target is still alive when we aim at it (currently aims at empty air if the target dies)
	// fix cache count not being correct
	// add localplayer headpos to cache, and gunpos if we can find that easily
	// combine all bounds associated with an entity before calculating its screen bounds, currently using just the body bounds makes the box very small, should add at least head extents and leg extents.
	if ((IsGameStateValid()) && (CacheGameData()))
	{
		if (shouldDoAimbot)
		{
			ESPTarget* target = GetBestAimTarget();
			if (target != NULL)
			{
				target->IsAimTargetThisFrame = true;
				if (target->HasVisibleHeadBone)
					AimAt(target->HeadBonePosition_WorldSpace);
				else
					AimAt(target->BodyBonePosition_WorldSpace);
			}
		}
	}
}

void CheatMain::Render(ImDrawList* drawList)
{
	for (int entIndex = 0; entIndex < NumEntitiesCached; ++entIndex)
	{
		if (entIndex >= Entities.size())
		{
			printf("Fatal error, Render() looped beyond Entities.size\n");
			return;
		}
		ESPTarget ent = Entities.at(entIndex);
		if (!ent.IsOnScreen) continue;

		ImColor espColor = ImColor(1.f, 0.f, 0.f, 1.f);
		if (ent.IsTeammate)
			espColor = ImColor(0.f, 1.f, 0.f, 1.f);
		if (ent.IsAimTargetThisFrame)
			espColor = ImColor(1.f, 0.f, 1.f, 1.f);

		if (ent.HasBounds)
		{
			// furthest face
			drawList->AddQuad(
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackTopLeft].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackTopLeft].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackTopRight].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackTopRight].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackBottomRight].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackBottomRight].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackBottomLeft].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackBottomLeft].y),
				espColor
			);
			// nearest face
			drawList->AddQuad(
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontTopLeft].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontTopLeft].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontTopRight].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontTopRight].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontBottomRight].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontBottomRight].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontBottomLeft].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontBottomLeft].y),
				espColor
			);
			// left face
			drawList->AddQuad(
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackTopLeft].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackTopLeft].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontTopLeft].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontTopLeft].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontBottomLeft].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontBottomLeft].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackBottomLeft].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackBottomLeft].y),
				espColor
			);
			// right face
			drawList->AddQuad(
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackTopRight].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackTopRight].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontTopRight].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontTopRight].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontBottomRight].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::FrontBottomRight].y),
				ImVec2(ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackBottomRight].x, ent.BoundsPoints_ScreenSpace[(int)BoundsPoint::BackBottomRight].y),
				espColor
			);
		}

		drawList->AddText(ImVec2(ent.NamePosition_ScreenSpace.x, ent.NamePosition_ScreenSpace.y), espColor, ent.Name.c_str());
	}
}

bool CheatMain::CacheGameData()
{
	if (CheatMain::ColliderType == NULL)
	{
		String* colTypeStr = (app::String*)il2cpp_string_new("UnityEngine.Collider, UnityEngine.PhysicsModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		CheatMain::ColliderType = app::Type_GetType_4(colTypeStr, nullptr);
		if (CheatMain::ColliderType == NULL)
		{
			printf("Could not get Collider type via reflection, cannot update bounds\n");
			return false;
		}
	}

	Camera* cam = app::Camera_get_main(nullptr);
	if (cam == NULL) return false;
	MainCamTrans = app::Component_1_get_transform((Component_1*)cam, nullptr);
	int32_t pixelWidth = app::Camera_get_pixelWidth(cam, nullptr);
	int32_t pixelHeight = app::Camera_get_pixelHeight(cam, nullptr);
	MainCamPixelDimensions = { (float)pixelWidth, (float)pixelHeight };

	Matrix4x4 worldToCameraMat = app::Camera_get_worldToCameraMatrix(cam, nullptr);
	Matrix4x4 projMat = app::Camera_get_projectionMatrix(cam, nullptr);
	MVPMatrix = utils::MultiplyMatrices(projMat, worldToCameraMat);

	auto gameShell = (*app::GameManager__TypeInfo)->static_fields->hM_PoZp;
	LocalPlayerTrans = gameShell->fields.tLROmNz;
	if (LocalPlayerTrans == NULL) return false;
	LocalPlayerPos = app::Transform_get_position(LocalPlayerTrans, nullptr);

	ScreenCenterPixelCoords = { app::Screen_get_width(nullptr) / 2, app::Screen_get_height(nullptr) / 2 };

	// now all that's left is to iterate the entities and populate ESPTarget structs
	auto entities = (*app::NetworkManager__TypeInfo)->static_fields->rXxulNQ->fields.nWu007Fobcq;
	NumEntitiesCached = 0;
	if ((entities) && (entities->fields.entries) && (entities->fields.entries->vector))
	{
		for (int entityIndex = 0; entityIndex < entities->fields.count; entityIndex++)
		{
			NetworkEntity* entity = entities->fields.entries->vector[entityIndex].value;
			if (entity)
			{
				if (entity->fields.Xu0080dQhsV) // <-- skip localplayer
					continue;
				if (entity->fields.BVuoKHw == NULL) continue;
				if (((SFSUser*)entity->fields.BVuoKHw)->fields.name == NULL) continue;
				Transform* entityTrans = app::Component_1_get_transform((Component_1*)entity, nullptr);
				if (entityTrans == NULL) continue;
				GameObject* entityGO = app::Component_1_get_gameObject((Component_1*)entity, nullptr);
				if (entityGO == NULL) continue;

				if (entityIndex >= Entities.size())
				{
					Entities.push_back(ESPTarget{ 0 });
				}
				ESPTarget* cachedTarget = &Entities.data()[entityIndex];
				NumEntitiesCached++;
				// clear any previous data before filling it with new
				cachedTarget->IsAimTargetThisFrame = false;
				cachedTarget->HasBounds = false;
				cachedTarget->HasVisibleHeadBone = false;
				cachedTarget->HasVisibleBodyBone = false;

				// start filling in the new data
				cachedTarget->Name = il2cppi_to_string(((SFSUser*)entity->fields.BVuoKHw)->fields.name);
				cachedTarget->Position_WorldSpace = app::Transform_get_position(entityTrans, nullptr);
				cachedTarget->Distance_WorldSpace = utils::Vec3_Distance(LocalPlayerPos, cachedTarget->Position_WorldSpace);
				// NOTE: hard-coded for now
				cachedTarget->EyeHeight_WorldSpace = 2.45f;

				// get nametag screen position (and set if it's visible on screen - we can skip the expensive Bounds calculations below if it's not on screen)
				Vector3 entityPos = { cachedTarget->Position_WorldSpace.x, cachedTarget->Position_WorldSpace.y + cachedTarget->EyeHeight_WorldSpace, cachedTarget->Position_WorldSpace.z };
				cachedTarget->NamePosition_ScreenSpace = { 0 };
				cachedTarget->IsOnScreen = utils::WorldToScreen(MVPMatrix, entityPos, &cachedTarget->NamePosition_ScreenSpace, pixelWidth * 0.5f, pixelHeight * 0.5f);
				
				// bulky collider stuff..
				Component_1__Array* cols = app::GameObject_GetComponentsInChildren_1(entityGO, CheatMain::ColliderType, false, nullptr);
				for (int colIndex = 0; colIndex < cols->max_length; ++colIndex)
				{
					app::Collider* col = (app::Collider*)cols->vector[colIndex];
					if (col != NULL)
					{
						// Bounds are in worldspace already, thankfully.
						Bounds colBounds = app::Collider_get_bounds(col, nullptr);
						String* colName = app::Object_1_get_name((app::Object_1*)col, nullptr);
						if (colName == NULL)
						{
							printf("Could not get collider name\n");
							continue;
						}
						std::string colNameStr = il2cppi_to_string(colName);
						// colliders that interest us (they are all BoxColliders btw, confirm by calling utils::ObjToString):
						// "body_0"
						// "Head"
						// prepare screen bounding box around body_0
						if ((cachedTarget->IsOnScreen) && (colNameStr == "body_0"))
						{
							Vector3 boundsWorldPos[(int)CheatMain::BoundsPoint::MAX];
							boundsWorldPos[(int)CheatMain::BoundsPoint::BackBottomLeft] = { colBounds.m_Center.x - colBounds.m_Extents.x, colBounds.m_Center.y - colBounds.m_Extents.y, colBounds.m_Center.z + colBounds.m_Extents.z };
							boundsWorldPos[(int)CheatMain::BoundsPoint::BackBottomRight] = { colBounds.m_Center.x + colBounds.m_Extents.x, colBounds.m_Center.y - colBounds.m_Extents.y, colBounds.m_Center.z + colBounds.m_Extents.z };
							boundsWorldPos[(int)CheatMain::BoundsPoint::FrontBottomLeft] = { colBounds.m_Center.x - colBounds.m_Extents.x, colBounds.m_Center.y - colBounds.m_Extents.y, colBounds.m_Center.z - colBounds.m_Extents.z };
							boundsWorldPos[(int)CheatMain::BoundsPoint::FrontBottomRight] = { colBounds.m_Center.x + colBounds.m_Extents.x, colBounds.m_Center.y - colBounds.m_Extents.y, colBounds.m_Center.z - colBounds.m_Extents.z };
							boundsWorldPos[(int)CheatMain::BoundsPoint::BackTopLeft] = { colBounds.m_Center.x - colBounds.m_Extents.x, colBounds.m_Center.y + colBounds.m_Extents.y, colBounds.m_Center.z + colBounds.m_Extents.z };
							boundsWorldPos[(int)CheatMain::BoundsPoint::BackTopRight] = { colBounds.m_Center.x + colBounds.m_Extents.x, colBounds.m_Center.y + colBounds.m_Extents.y, colBounds.m_Center.z + colBounds.m_Extents.z };
							boundsWorldPos[(int)CheatMain::BoundsPoint::FrontTopLeft] = { colBounds.m_Center.x - colBounds.m_Extents.x, colBounds.m_Center.y + colBounds.m_Extents.y, colBounds.m_Center.z - colBounds.m_Extents.z };
							boundsWorldPos[(int)CheatMain::BoundsPoint::FrontTopRight] = { colBounds.m_Center.x + colBounds.m_Extents.x, colBounds.m_Center.y + colBounds.m_Extents.y, colBounds.m_Center.z - colBounds.m_Extents.z };
							for (int boundsIndex = 0; boundsIndex < (int)CheatMain::BoundsPoint::MAX; ++boundsIndex)
							{
								utils::WorldToScreen(MVPMatrix, boundsWorldPos[boundsIndex], &cachedTarget->BoundsPoints_ScreenSpace[boundsIndex], pixelWidth * 0.5f, pixelHeight * 0.5f);
							}
						}
						// raycast against all colliders and save the head bone and body bones separately if visible
						Vector3 diffPos = utils::Vec3_Subtract(colBounds.m_Center, LocalPlayerPos);
						utils::Vec3_ToNormalized(diffPos);
						app::RaycastHit hitInfo = { 0 };
						bool raycast = app::Physics_Raycast_4(LocalPlayerPos, diffPos, &hitInfo, 5000.f, -5, app::QueryTriggerInteraction__Enum::UseGlobal, nullptr);
						if (raycast)
						{
							app::Collider* hitCol = oRaycastHit_GetCollider(&hitInfo);
							if (hitCol != NULL)
							{
								Transform* hitTrans = app::Component_1_get_transform((Component_1*)hitCol, nullptr);
								Transform* hitTransRoot = app::Transform_get_root(hitTrans, nullptr);
								if (hitTransRoot == entityTrans)
								{
									cachedTarget->Distance_WorldSpace = hitInfo.m_Distance;
									String* hitColName = app::Object_1_get_name((app::Object_1*)hitCol, nullptr);
									if (hitColName == NULL)
									{
										printf("Could not get hit collider name\n");
										continue;
									}
									std::string hitColNameStr = il2cppi_to_string(hitColName);
									if (hitColNameStr == "Head")
									{
										cachedTarget->HeadBonePosition_WorldSpace = hitInfo.m_Point;
										cachedTarget->HasVisibleHeadBone = true;
									}
									else if (hitColNameStr == "body_0")
									{
										cachedTarget->BodyBonePosition_WorldSpace = hitInfo.m_Point;
										cachedTarget->HasVisibleBodyBone = true;
									}
									else if (cachedTarget->HasVisibleBodyBone == false)
									{
										cachedTarget->BodyBonePosition_WorldSpace = hitInfo.m_Point;
										cachedTarget->HasVisibleBodyBone = true;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return true;
}

CheatMain::ESPTarget* CheatMain::GetBestAimTarget()
{
	ESPTarget* closestTarget = NULL;
	float closestDistance = FLT_MAX;
	for (int entIndex = 0; entIndex < NumEntitiesCached; ++entIndex)
	{
		if (entIndex >= Entities.size())
		{
			printf("Fatal error, Render() looped beyond Entities.size\n");
			return closestTarget;
		}
		ESPTarget* ent = &Entities[entIndex];
		if (ent->Distance_WorldSpace < closestDistance)
		{
			closestDistance = ent->Distance_WorldSpace;
			closestTarget = ent;
		}
	}
	return closestTarget;
}

void CheatMain::AimAt(app::Vector3 worldPos)
{
	// TO-DO:
	// get proper gun position
	Vector3 gunPosition = { LocalPlayerPos.x, LocalPlayerPos.y + 2.45f, LocalPlayerPos.z };
	Vector3 aimAngle = utils::UnityCalcAngle(gunPosition, worldPos);
	// in unity, one common (but not universal...) FPS controller setup has the CharacterController on a root gameobject and the Camera component on a child gameobject
	// and the FPSController sets the Y-component of the root gameobject and the X-component of the Camera gameobject based on mouse axes
	app::Transform_set_eulerAngles(LocalPlayerTrans, Vector3{ 0.f, aimAngle.y, 0.f }, nullptr);
	app::Transform_set_localEulerAngles(MainCamTrans, Vector3{ aimAngle.x, 0.f, 0.f }, nullptr);
}

bool CheatMain::IsGameStateValid()
{
	// begin checking if game is in a valid state for us to draw ESP
	// in games made by actual programmers, this process can be a simple one-liner.
	// but this game is different, and state is kept all over the place.
	// PS for gamedevs: frequent, new bugs (make one change here and something breaks there) are a sign of state not being managed well enough!
	auto gameState = (*app::GameManager__TypeInfo)->static_fields->STLu007FpPR;
	if (gameState != app::ft_ru0082XR__Enum::InProgress) return false;

	auto gameShell = (*app::GameManager__TypeInfo)->static_fields->hM_PoZp;
	if ((gameShell == NULL) || (gameShell->fields._MV_Z_G == false) || (gameShell->fields.iiI_u0081NN == true)) return false;

	auto gameManagerInst = (*app::GameManager__TypeInfo)->static_fields->rXxulNQ;
	if (gameManagerInst == NULL) return false;
	auto localPlayerGO = gameManagerInst->fields.Zqu007FaiVl;
	if (localPlayerGO == NULL) return false;
	auto smartfoxNet = gameManagerInst->fields.ckRKdUk;
	if (smartfoxNet == NULL) return false;
	auto localNetUser = smartfoxNet->fields.mySelf;
	if (localNetUser == NULL) return false;
	auto netRoomManager = smartfoxNet->fields.roomManager;
	if (netRoomManager == NULL) return false;
	// TO-DO:
	// figure out why SFSRoomManager_GetJoinedRooms crashes even when the netRoomManager is valid. it's like 2 or 3 calls deep into that call.
	/*
	printf("NetRoomManager: %llx\n", netRoomManager);
	auto joinedRoomList = app::SFSRoomManager_GetJoinedRooms((SFSRoomManager*)netRoomManager, nullptr);
	printf("joinedRoomList: %llx\n", joinedRoomList);
	if (joinedRoomList == NULL || joinedRoomList->fields._size < 1) return false;
	*/
	// end checking if game is in a valid state for us to draw ESP
	return true;
}