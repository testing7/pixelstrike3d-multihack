#pragma once

#include "il2cpp-appdata.h"

#define PI    (3.141592654f)
#define Rad2Deg (57.295779513082320876798154814105f)

class utils
{
	public:
		utils() = delete;
		utils(const utils&) = delete;
		utils& operator=(const utils& o) = delete;
		~utils() = delete;


		static app::Vector3 MultiplyMatrixByVec3(app::Matrix4x4& mat, app::Vector3& vec);
		static app::Matrix4x4 MultiplyMatrices(app::Matrix4x4& lhs, app::Matrix4x4& rhs);
		static app::Vector3 Vec3_Subtract(app::Vector3& lhs, app::Vector3& rhs);
		static app::Vector3 Vec3_MultF(app::Vector3& lhs, float rhs);
		static float Vec3_Magnitude(app::Vector3& vec);
		static void Vec3_ToNormalized(app::Vector3& refVec);
		static float Vec3_Distance(app::Vector3& lhs, app::Vector3& rhs);

		static bool WorldToScreen(app::Matrix4x4& mvp, app::Vector3& worldPos, app::Vector3* screenPos, float halfPixelWidth, float halfPixelHeight);
		static app::Vector3 UnityCalcAngle(app::Vector3& src, app::Vector3& dst);
		static app::Vector2Int ClampMouseDelta(app::Vector2Int& delta);

		// calls ToString() virtual method, works for all .NET reference types
		static app::String* ObjToString(app::Object* obj);

		// game-specific. checks if game is in a valid state for drawing / raycasting.
		static bool IsGameStateValid();
};