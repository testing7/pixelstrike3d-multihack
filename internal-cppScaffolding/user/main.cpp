// Generated C++ file by Il2CppInspector - http://www.djkaty.com - https://github.com/djkaty
// Custom injected code entry point
#include "pch-il2cpp.h"

#include "il2cpp-appdata.h"
#include "helpers.h"

#include "main.h"
#include "../user/utils.h"
#include "../user/Input.h"
#include "../user/CheatMain.h"


using namespace app;

// Set the name of your log file here
extern const LPCWSTR LOG_FILE = L"ps3d-Multihack-log.txt";

void InitImGUI();

// TO-DO:
/* needs to be updated. this is from like 2010. i'll do it the boring way and create the device and whatnot for now.
// d3d11.dll
#define DEVICECONTEXT_VTABLE_PATTERN "\x0F\xB6\x83\x6B\x0E\x00\x00\x48\x8B\xCF\x48\x69\xD0\xA8\x04\x00\x00\x48\x8D\x05\x00\x00\x00\x00"
#define DEVICECONTEXT_VTABLE_MASK "xxxxxxxxxxxxxxxxxxxx????"
// dxgi.dll
#define SWAPCHAIN_VTABLE_PATTERN "\x48\x8D\x05\x00\x00\x00\x00\x48\x89\x07\x48\x8D\x05\x00\x00\x00\x00\x48\x89\x47\x08\x48\x8D\x05\x00\x00\x00\x00\x48\x89\x47\x10\x48\x8D\x05\x00\x00\x00\x00\x48\x89\x47\x78"
#define SWAPCHAIN_VTABLE_MASK "xxx????xxxxxx????xxxxxxx????xxxxxxx????xxxx"
*/
PDWORD64 swapChainVTable = NULL;
// just keeping these around in case we want to do something with them
PDWORD64 deviceContextVTable = NULL;
PDWORD64 deviceVTable = NULL;

typedef HRESULT(__stdcall* dPresent)(IDXGISwapChain* SwapChain, UINT SyncInterval, UINT Flags);
dPresent oPresent = NULL;

typedef void(__thiscall* dMonoBehaviour_CallUpdateMethod)(MonoBehaviour* _this, int updateMode);
dMonoBehaviour_CallUpdateMethod oMonoBehaviour_CallUpdateMethod = NULL;

HWND window = NULL;
WNDPROC oWndProc;
ID3D11Device* pDevice = NULL;
ID3D11DeviceContext* pContext = NULL;
ID3D11RenderTargetView* mainRenderTargetView;

CheatMain* _Cheat = NULL;

bool shouldDoAimbot = false;

LRESULT __stdcall WndProc(const HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{
    // TO-DO:
    // check if imgui wants input
    // check our own input too

    return CallWindowProc(oWndProc, hWnd, uMsg, wParam, lParam);
}

unsigned long long frameCount = 0; // incremented in hkPresent
unsigned long long updateCount = 0; // incremented in hkMonoBehaviour_CallUpdateMethod only when frameCount is greater or equal
// effectively, we call DoAimbot() once per frame even though hkMonoBehaviour_CallUpdateMethod is called like a billion times per frame.
void hkMonoBehaviour_CallUpdateMethod(MonoBehaviour* _this, int updateMode)
{
    if ((updateCount <= frameCount) && (utils::IsGameStateValid()))
    {
        _Cheat->PerFrameUpdate(shouldDoAimbot);
        updateCount++;
    }

    oMonoBehaviour_CallUpdateMethod(_this, updateMode);
}

bool inited = false;
HRESULT hkPresent(IDXGISwapChain* SwapChain, UINT SyncInterval, UINT Flags)
{
    frameCount++;
    // CREDIT: imgui boilerplate & init code yoink'd from rdbo [via https://github.com/rdbo/ImGui-DirectX-11-Kiero-Hook]
    if (inited == false)
    {
        if (SUCCEEDED(SwapChain->GetDevice(__uuidof(ID3D11Device), (void**)&pDevice)))
        {
            pDevice->GetImmediateContext(&pContext);
            DXGI_SWAP_CHAIN_DESC sd;
            SwapChain->GetDesc(&sd);
            window = sd.OutputWindow;
            ID3D11Texture2D* pBackBuffer;
            SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
            pDevice->CreateRenderTargetView(pBackBuffer, NULL, &mainRenderTargetView);
            pBackBuffer->Release();
            oWndProc = (WNDPROC)SetWindowLongPtr(window, GWLP_WNDPROC, (LONG_PTR)WndProc);
            InitImGUI();
            inited = true;
        }
        else
        {
            il2cppi_log_write("ERROR: Could not get device from swapchain");
            return oPresent(SwapChain, SyncInterval, Flags);
        }
    }
    ImGui_ImplDX11_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();

    ImDrawList* overlayDrawList = ImGui::GetOverlayDrawList();
    overlayDrawList->AddText(ImVec2(10.f, 10.f), ImColor(1.f, 0.f, 1.f), "Hello world");
    std::ostringstream oss;
    oss << "Numpad2 to toggle Aimbot!\nAimbot: " << shouldDoAimbot;
    overlayDrawList->AddText(ImVec2(10.f, 30.f), ImColor(1.f, 0.f, 1.f), oss.str().c_str());
    if (utils::IsGameStateValid())
    {
        _Cheat->Render(overlayDrawList);
    }

    ImGui::Render();
    pContext->OMSetRenderTargets(1, &mainRenderTargetView, NULL);
    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
    return oPresent(SwapChain, SyncInterval, Flags);
}

void InitImGUI()
{
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.ConfigFlags = ImGuiConfigFlags_NoMouseCursorChange;
    ImGui_ImplWin32_Init(window);
    ImGui_ImplDX11_Init(pDevice, pContext);
}

void InitD3D11()
{
    // CREDIT:
    // window / d3d11 init code yoink'd from kiero [via https://github.com/Rebzzel/kiero]
    WNDCLASSEX windowClass;
    windowClass.cbSize = sizeof(WNDCLASSEX);
    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = DefWindowProc;
    windowClass.cbClsExtra = 0;
    windowClass.cbWndExtra = 0;
    windowClass.hInstance = GetModuleHandle(NULL);
    windowClass.hIcon = NULL;
    windowClass.hCursor = NULL;
    windowClass.hbrBackground = NULL;
    windowClass.lpszMenuName = NULL;
    windowClass.lpszClassName = L"PixelStrike3D - DummyWndClass";
    windowClass.hIconSm = NULL;

    RegisterClassEx(&windowClass);

    HWND window = CreateWindowW(windowClass.lpszClassName, L"PixelStrike3D - DummyWndClass", WS_OVERLAPPEDWINDOW, 0, 0, 100, 100, NULL, NULL, windowClass.hInstance, NULL);

    HMODULE libD3D11;
    if ((libD3D11 = GetModuleHandle(L"d3d11.dll")) == NULL)
    {
        DestroyWindow(window);
        UnregisterClass(windowClass.lpszClassName, windowClass.hInstance);
        return;
    }

    void* D3D11CreateDeviceAndSwapChainFunc;
    if ((D3D11CreateDeviceAndSwapChainFunc = GetProcAddress(libD3D11, "D3D11CreateDeviceAndSwapChain")) == NULL)
    {
        DestroyWindow(window);
        UnregisterClass(windowClass.lpszClassName, windowClass.hInstance);
        return;
    }

    D3D_FEATURE_LEVEL featureLevel;
    const D3D_FEATURE_LEVEL featureLevels[] = { D3D_FEATURE_LEVEL_10_1, D3D_FEATURE_LEVEL_11_0 };

    DXGI_RATIONAL refreshRate;
    refreshRate.Numerator = 60;
    refreshRate.Denominator = 1;

    DXGI_MODE_DESC bufferDesc;
    bufferDesc.Width = 100;
    bufferDesc.Height = 100;
    bufferDesc.RefreshRate = refreshRate;
    bufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

    DXGI_SAMPLE_DESC sampleDesc;
    sampleDesc.Count = 1;
    sampleDesc.Quality = 0;

    DXGI_SWAP_CHAIN_DESC swapChainDesc;
    swapChainDesc.BufferDesc = bufferDesc;
    swapChainDesc.SampleDesc = sampleDesc;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.BufferCount = 1;
    swapChainDesc.OutputWindow = window;
    swapChainDesc.Windowed = 1;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

    IDXGISwapChain* swapChain;
    ID3D11Device* device;
    ID3D11DeviceContext* context;

    if (((long(__stdcall*)(
        IDXGIAdapter*,
        D3D_DRIVER_TYPE,
        HMODULE,
        UINT,
        const D3D_FEATURE_LEVEL*,
        UINT,
        UINT,
        const DXGI_SWAP_CHAIN_DESC*,
        IDXGISwapChain**,
        ID3D11Device**,
        D3D_FEATURE_LEVEL*,
        ID3D11DeviceContext**))(D3D11CreateDeviceAndSwapChainFunc))(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, featureLevels, 1, D3D11_SDK_VERSION, &swapChainDesc, &swapChain, &device, &featureLevel, &context) < 0)
    {
        DestroyWindow(window);
        UnregisterClass(windowClass.lpszClassName, windowClass.hInstance);
        return;
    }

    swapChainVTable = *(PDWORD64*)swapChain;
    deviceContextVTable = *(PDWORD64*)context;
    deviceVTable = *(PDWORD64*)device;

    // ghetto vtable hook, should freeze threads and call virtualprotect first to avoid crashes
    DWORD dwProtect;
    VirtualProtect(swapChainVTable, (8 + 1) * 8, PAGE_EXECUTE_READWRITE, &dwProtect);
    oPresent = (dPresent)swapChainVTable[8];
    swapChainVTable[8] = (DWORD64)&hkPresent;
    VirtualProtect(swapChainVTable, (8 + 1) * 8, dwProtect, NULL);

    swapChain->Release();
    swapChain = NULL;

    device->Release();
    device = NULL;

    context->Release();
    context = NULL;

    DestroyWindow(window);
    UnregisterClass(windowClass.lpszClassName, windowClass.hInstance);
}

// Custom injected code entry point
void Run()
{
    // Initialize thread data - DO NOT REMOVE
    il2cpp_thread_attach(il2cpp_domain_get());

    // If you would like to write to a log file, specify the name above and use il2cppi_log_write()
    // il2cppi_log_write("Startup");

    // If you would like to output to a new console window, use il2cppi_new_console() to open one and redirect stdout
    il2cppi_new_console();

    // Place your custom code here
    // 
    // TO-DO:
    // hook Scripting::UnityEngine::GUIUtilityProxy::BeginGUI engine func (not exposed via il2cpp)
    InitD3D11();

    _Cheat = new CheatMain();

    std::uintptr_t unityPlayerAddr = (std::uintptr_t)GetModuleHandleA("UnityPlayer.dll");
    if (unityPlayerAddr != 0)
    {
        std::uintptr_t funcAddr = unityPlayerAddr + 0x843A60;
        MH_STATUS mhStatus;
        mhStatus = MH_Initialize();
        if (mhStatus == MH_OK)
        {
            mhStatus = MH_CreateHookEx((LPVOID)funcAddr, &hkMonoBehaviour_CallUpdateMethod, &oMonoBehaviour_CallUpdateMethod);
            if (mhStatus == MH_OK)
            {
                mhStatus = MH_EnableHook((LPVOID)funcAddr);
                if (mhStatus != MH_OK)
                {
                    il2cppi_log_write("Could not enable hook");
                }
            }
            else
            {
                il2cppi_log_write("Could not create hook");
            }
        }
        else
        {
            il2cppi_log_write("Could not initialize minhook");
        }
    }
    else
    {
        il2cppi_log_write("Could not get GameAssembly.dll base address");
    }

    // TO-DO:
    // remove this, and move clean-up logic to WndProc handler / Eject function / DllMain
    // this Run() function needs to exit otherwise it'll crash after a while
    // unless the Present hook is what's causing the crash, then we should call il2cpp_thread_attach(il2cpp_domain_get()); as part of Present's init
    while (true)
    {
        if (Input::IsKeyPressed(VK_NUMPAD1))
        {
            ::Beep(800, 400);
        }
        if (Input::IsKeyPressed(VK_NUMPAD2))
        {
            shouldDoAimbot = !shouldDoAimbot;
        }
    }
    if (mainRenderTargetView)
    {
        mainRenderTargetView->Release();
        mainRenderTargetView = 0;
        ImGui_ImplDX11_Shutdown();
        ImGui_ImplWin32_Shutdown();
        ImGui::DestroyContext();
    }
}