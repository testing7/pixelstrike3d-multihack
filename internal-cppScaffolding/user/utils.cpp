#include "pch-il2cpp.h"

#include "utils.h"

#include <cmath>

using namespace app;


app::String* utils::ObjToString(app::Object* obj)
{
    auto toStringFPtr = (app::String * (*)(app::Object*, MethodInfo*)) obj->klass->vtable.ToString.methodPtr;
    MethodInfo* toStringMethodInfo = (MethodInfo*)obj->klass->vtable.ToString.method;
    app::String* result = toStringFPtr(obj, toStringMethodInfo);
    //printf("Calling ToString @ [%llx] for (%s)\n", toStringFPtr, il2cppi_to_string(result).c_str());
    return result;
}

bool utils::IsGameStateValid()
{
    // begin checking if game is in a valid state for us to draw ESP
    // in games made by actual programmers, this process can be a simple one-liner.
    // but this game is different, and state is kept all over the place.
    // PS for gamedevs: frequent, new bugs (make one change here and something breaks there) are a sign of state not being managed well enough!
    auto gameState = (*app::GameManager__TypeInfo)->static_fields->STLu007FpPR;
    if (gameState != app::ft_ru0082XR__Enum::InProgress) return false;

    auto gameShell = (*app::GameManager__TypeInfo)->static_fields->hM_PoZp;
    if ((gameShell == NULL) || (gameShell->fields._MV_Z_G == false) || (gameShell->fields.iiI_u0081NN == true)) return false;

    auto gameManagerInst = (*app::GameManager__TypeInfo)->static_fields->rXxulNQ;
    if (gameManagerInst == NULL) return false;
    auto localPlayerGO = gameManagerInst->fields.Zqu007FaiVl;
    if (localPlayerGO == NULL) return false;
    auto smartfoxNet = gameManagerInst->fields.ckRKdUk;
    if (smartfoxNet == NULL) return false;
    auto localNetUser = smartfoxNet->fields.mySelf;
    if (localNetUser == NULL) return false;
    auto netRoomManager = smartfoxNet->fields.roomManager;
    if (netRoomManager == NULL) return false;
    // TO-DO:
    // figure out why SFSRoomManager_GetJoinedRooms crashes even when the netRoomManager is valid. it's like 2 or 3 calls deep into that call.
    /*
    printf("NetRoomManager: %llx\n", netRoomManager);
    auto joinedRoomList = app::SFSRoomManager_GetJoinedRooms((SFSRoomManager*)netRoomManager, nullptr);
    printf("joinedRoomList: %llx\n", joinedRoomList);
    if (joinedRoomList == NULL || joinedRoomList->fields._size < 1) return false;
    */
    // end checking if game is in a valid state for us to draw ESP
    return true;
}

// credit: https://www.unknowncheats.me/forum/other-fps-games/157670-blockade-3d-aimbot.html
app::Vector3 utils::UnityCalcAngle(app::Vector3& src, app::Vector3& dst)
{
    Vector3 diff = utils::Vec3_Subtract(dst, src);
    float dist = utils::Vec3_Distance(src, dst);

    // can any mathemagicians explain why this works in Unity when the traditional CalcAngle method doesn't?
    // traditional CalcAngle:
    // angles.x = -Mathf.Atan2(diff.x, diff.y) * Rad2Deg + 180.0f;
    // angles.y = Mathf.Asin(diff.z / distance) * Rad2Deg;

    float xzLength = sqrtf((diff.x * diff.x) + (diff.z * diff.z));

    float angleX = atan2f(diff.y, xzLength) * -Rad2Deg;
    float angleY = atan2f(diff.x, diff.z) * Rad2Deg;

    return Vector3{ angleX, angleY, 0.f };
}

app::Vector2Int utils::ClampMouseDelta(Vector2Int& delta)
{
    Vector2Int clampedDelta = Vector2Int{ delta.m_X, delta.m_Y };
    int halfScreenWidth = app::Screen_get_width(nullptr) / 2;
    int halfScreenHeight = app::Screen_get_height(nullptr) / 2;

    if (delta.m_X > halfScreenWidth)
        clampedDelta.m_X = halfScreenWidth;
    if (delta.m_Y > halfScreenHeight)
        clampedDelta.m_Y = halfScreenHeight;
    if (delta.m_X < -halfScreenWidth)
        clampedDelta.m_X = -halfScreenWidth;
    if (delta.m_Y < -halfScreenHeight)
        clampedDelta.m_Y = -halfScreenHeight;

    return clampedDelta;
}

app::Matrix4x4 utils::MultiplyMatrices(app::Matrix4x4& lhs, app::Matrix4x4& rhs)
{
    app::Matrix4x4 matrixx = app::Matrix4x4();
    matrixx.m00 = (((lhs.m00 * rhs.m00) + (lhs.m01 * rhs.m10)) + (lhs.m02 * rhs.m20)) + (lhs.m03 * rhs.m30);
    matrixx.m01 = (((lhs.m00 * rhs.m01) + (lhs.m01 * rhs.m11)) + (lhs.m02 * rhs.m21)) + (lhs.m03 * rhs.m31);
    matrixx.m02 = (((lhs.m00 * rhs.m02) + (lhs.m01 * rhs.m12)) + (lhs.m02 * rhs.m22)) + (lhs.m03 * rhs.m32);
    matrixx.m03 = (((lhs.m00 * rhs.m03) + (lhs.m01 * rhs.m13)) + (lhs.m02 * rhs.m23)) + (lhs.m03 * rhs.m33);
    matrixx.m10 = (((lhs.m10 * rhs.m00) + (lhs.m11 * rhs.m10)) + (lhs.m12 * rhs.m20)) + (lhs.m13 * rhs.m30);
    matrixx.m11 = (((lhs.m10 * rhs.m01) + (lhs.m11 * rhs.m11)) + (lhs.m12 * rhs.m21)) + (lhs.m13 * rhs.m31);
    matrixx.m12 = (((lhs.m10 * rhs.m02) + (lhs.m11 * rhs.m12)) + (lhs.m12 * rhs.m22)) + (lhs.m13 * rhs.m32);
    matrixx.m13 = (((lhs.m10 * rhs.m03) + (lhs.m11 * rhs.m13)) + (lhs.m12 * rhs.m23)) + (lhs.m13 * rhs.m33);
    matrixx.m20 = (((lhs.m20 * rhs.m00) + (lhs.m21 * rhs.m10)) + (lhs.m22 * rhs.m20)) + (lhs.m23 * rhs.m30);
    matrixx.m21 = (((lhs.m20 * rhs.m01) + (lhs.m21 * rhs.m11)) + (lhs.m22 * rhs.m21)) + (lhs.m23 * rhs.m31);
    matrixx.m22 = (((lhs.m20 * rhs.m02) + (lhs.m21 * rhs.m12)) + (lhs.m22 * rhs.m22)) + (lhs.m23 * rhs.m32);
    matrixx.m23 = (((lhs.m20 * rhs.m03) + (lhs.m21 * rhs.m13)) + (lhs.m22 * rhs.m23)) + (lhs.m23 * rhs.m33);
    matrixx.m30 = (((lhs.m30 * rhs.m00) + (lhs.m31 * rhs.m10)) + (lhs.m32 * rhs.m20)) + (lhs.m33 * rhs.m30);
    matrixx.m31 = (((lhs.m30 * rhs.m01) + (lhs.m31 * rhs.m11)) + (lhs.m32 * rhs.m21)) + (lhs.m33 * rhs.m31);
    matrixx.m32 = (((lhs.m30 * rhs.m02) + (lhs.m31 * rhs.m12)) + (lhs.m32 * rhs.m22)) + (lhs.m33 * rhs.m32);
    matrixx.m33 = (((lhs.m30 * rhs.m03) + (lhs.m31 * rhs.m13)) + (lhs.m32 * rhs.m23)) + (lhs.m33 * rhs.m33);
    return matrixx;
}

app::Vector3 utils::MultiplyMatrixByVec3(app::Matrix4x4& mat, app::Vector3& vec)
{
    app::Vector4 vec4 = app::Vector4{ vec.x, vec.y, vec.z, 1.f };
    app::Vector3 result = app::Vector3();
    result.x = (((mat.m00 * vec4.x) + (mat.m01 * vec4.y)) + (mat.m02 * vec4.z)) + (mat.m03 * vec4.w);
    result.y = (((mat.m10 * vec4.x) + (mat.m11 * vec4.y)) + (mat.m12 * vec4.z)) + (mat.m13 * vec4.w);
    //result.z = (((mat.m20 * vec4.x) + (mat.m21 * vec4.y)) + (mat.m22 * vec4.z)) + (mat.m23 * vec4.w);
    //result.w = (((mat.m30 * vec4.x) + (mat.m31 * vec4.y)) + (mat.m32 * vec4.z)) + (mat.m33 * vec4.w);
    result.z = (((mat.m30 * vec4.x) + (mat.m31 * vec4.y)) + (mat.m32 * vec4.z)) + (mat.m33 * vec4.w);
    return result;
}

app::Vector3 utils::Vec3_Subtract(app::Vector3& lhs, app::Vector3& rhs)
{
    app::Vector3 ret = app::Vector3{ lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z };
    return ret;
}

app::Vector3 utils::Vec3_MultF(app::Vector3& lhs, float rhs)
{
    app::Vector3 ret = app::Vector3{ lhs.x * rhs, lhs.y * rhs, lhs.z * rhs };
    return ret;
}

float utils::Vec3_Magnitude(app::Vector3& vec)
{
    return sqrtf((float)((double)vec.x * (double)vec.x + (double)vec.y * (double)vec.y + (double)vec.z * (double)vec.z));
}

void utils::Vec3_ToNormalized(app::Vector3& refVec)
{
    float num = Vec3_Magnitude(refVec);
    if ((double)num > 9.99999974737875E-06)
    {
        refVec.x = refVec.x / num;
        refVec.y = refVec.y / num;
        refVec.z = refVec.z / num;
    }
    else
    {
        refVec.x = 0.f;
        refVec.y = 0.f;
        refVec.z = 0.f;
    }
}

float utils::Vec3_Distance(app::Vector3& lhs, app::Vector3& rhs)
{
    app::Vector3 ret = Vec3_Subtract(rhs, lhs);
    return Vec3_Magnitude(ret);
}

bool utils::WorldToScreen(Matrix4x4& mvp, Vector3& worldPos, Vector3* screenPos, float halfPixelWidth, float halfPixelHeight)
{
    bool onScreen = true;
    *screenPos = utils::MultiplyMatrixByVec3(mvp, worldPos);

    if (screenPos->z < 0.1f)
        onScreen = false;

    // perspective divide
    screenPos->x = (screenPos->x / screenPos->z);
    screenPos->y = (screenPos->y / screenPos->z);
    // transform to screen coords (0,0 is top left; pixelWidth,pixelHeight is bottom right)
    screenPos->x = (screenPos->x * halfPixelWidth) + (screenPos->x + halfPixelWidth);
    screenPos->y = -(screenPos->y * halfPixelHeight) + (screenPos->y + halfPixelHeight);

    return onScreen;
}