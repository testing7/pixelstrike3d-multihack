#pragma once

#include <Windows.h>

class Input
{
	static bool bKeyLock[VK_OEM_CLEAR];
	static bool bKeyDown[VK_OEM_CLEAR];
	static DWORD dwKeyTime[VK_OEM_CLEAR];

public:
	static bool IsKeyPressed(int vKey, bool bAllowKeyHeld = false);
};

