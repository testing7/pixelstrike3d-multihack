#pragma once

#include "il2cpp-appdata.h"

#include "imgui/imgui.h"
#include <string>
#include <vector>

class CheatMain
{
private:
	// define the function definition for RaycastHit_GetCollider
	typedef app::Collider* (__thiscall* dRaycastHit_GetCollider)(app::RaycastHit* _this);

	enum class BoundsPoint : int
	{
		BackBottomLeft,
		BackBottomRight,
		FrontBottomLeft,
		FrontBottomRight,
		BackTopLeft,
		BackTopRight,
		FrontTopLeft,
		FrontTopRight,
		MAX
	};

	// define the struct we'll use to hold per-player cached data
	// we want this private because no other code should need to touch this
	struct ESPTarget
	{
		float Distance_WorldSpace;
		app::Vector3 BoundsPoints_ScreenSpace[(int)CheatMain::BoundsPoint::MAX];
		app::Vector3 NamePosition_ScreenSpace;
		bool HasBounds;
		bool IsOnScreen;
		bool IsTeammate; 

		bool HasVisibleHeadBone;
		bool HasVisibleBodyBone;
		bool IsAimTargetThisFrame; // <-- checked in the Render method to draw it as a different color
		app::Vector3 HeadBonePosition_WorldSpace;
		app::Vector3 BodyBonePosition_WorldSpace;
		app::Vector3 Position_WorldSpace;
		float EyeHeight_WorldSpace;

		std::string Name;
	};

public:
	CheatMain();
	// these two are called from the hooks in main.cpp.
	// i like the idea of having them slightly decoupled: the main idea is still visible from reading just main.cpp while the hooks can be swapped out without rewriting anything in here
	void PerFrameUpdate(bool shouldDoAimbot);
	void Render(ImDrawList* drawList);

private:
	// checks to see if the game is in a suitable state for drawing / accessing certain pointers, called from PerFrameUpdate() and Render().
	bool IsGameStateValid();
	// grabs all relevant data from the game, called from PerFrameUpdate()
	// the idea is to only touch the game data once, and then cache it so we can reuse it from any point in our cheat w/out having to call game functions again.
	// we also do some per-player processing, like getting the username, which bone (if any) is best for aiming at, what the draw color should be, etc. and save all that to a ESPTarget struct.
	bool CacheGameData();

	// iterates our ESPTarget's and decides which one is the best candidate for our aimbot. called from PerFrameUpdate().
	// separating this logic out to its own function like this allows us to easily switch to new target-selection logic without touching the main aimbot code
	// NOTE:
	// returning a pointer to an element in a vector is bad, the vector may re-allocate and move all elements to a new address, thus invalidating the pointer.
	// but we have tight control over the vector operations so it shouldn't be too bad
	ESPTarget* GetBestAimTarget();

	// the core of the aimbot, called by PerFrameUpdate()
	// quite simply writes aim angles to the localplayer's transform
	void AimAt(app::Vector3 worldPos);

private:
	dRaycastHit_GetCollider oRaycastHit_GetCollider = NULL;
	static app::Type* ColliderType;
	std::vector<ESPTarget> Entities;
	int NumEntitiesCached;

	// per-frame state data that we cache from the game
	app::Transform* LocalPlayerTrans;
	app::Vector3 LocalPlayerPos;

	app::Transform* MainCamTrans;
	app::Vector2 MainCamPixelDimensions; // <-- in almost all cases, this should be the same as ScreenDimensions, but just in case
	app::Matrix4x4 MVPMatrix;

	// fields specifically for the mouse aimbot
	app::Vector2Int ScreenCenterPixelCoords;
	app::Vector2Int LastMouseDelta;

};