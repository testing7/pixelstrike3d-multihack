#include "pch-il2cpp.h"
#include "Input.h"

bool Input::bKeyLock[VK_OEM_CLEAR] = { 0 };
bool Input::bKeyDown[VK_OEM_CLEAR] = { 0 };
DWORD Input::dwKeyTime[VK_OEM_CLEAR] = { 0 };

// credit: NeoIII (http://www.unknowncheats.me/forum/battlefield-4/104771-catch-keyboard-input.html)
/*static*/ bool Input::IsKeyPressed(int vKey, bool bAllowKeyHeld /*= false*/)
{
    bKeyDown[vKey] = !!(GetAsyncKeyState(vKey) & 0x8000);
    DWORD dwCurTime = GetTickCount();

    if (!bKeyDown[vKey] || (bAllowKeyHeld && dwKeyTime[vKey] < dwCurTime))
        bKeyLock[vKey] = false;

    if (bKeyDown[vKey] && !bKeyLock[vKey])
    {
        dwKeyTime[vKey] = dwCurTime + 300;
        bKeyLock[vKey] = true;
        return true;
    }
    return false;
}