// Generated C++ file by Il2CppInspector - http://www.djkaty.com - https://github.com/djkaty
// Target Unity version: 2020.1.11 - 2020.1.17

// ******************************************************************************
// * IL2CPP application-specific method definition addresses and signatures
// ******************************************************************************

using namespace app;

DO_APP_FUNC(0x00E89C70, Type*, Type_GetType_4, (String* typeName, MethodInfo* method));

DO_APP_FUNC(0x00F84AF0, String*, Object_1_get_name, (Object_1* __this, MethodInfo* method));

DO_APP_FUNC(0x00FA2700, Camera*, Camera_get_main, (MethodInfo* method));
DO_APP_FUNC(0x00FA2A40, int32_t, Camera_get_pixelWidth, (Camera* __this, MethodInfo* method));
DO_APP_FUNC(0x00FA2950, int32_t, Camera_get_pixelHeight, (Camera* __this, MethodInfo* method));
DO_APP_FUNC(0x00FA3400, Matrix4x4, Camera_get_worldToCameraMatrix, (Camera* __this, MethodInfo* method));
DO_APP_FUNC(0x00FA2BF0, Matrix4x4, Camera_get_projectionMatrix, (Camera* __this, MethodInfo* method));

DO_APP_FUNC(0x00FA8C30, Transform*, Component_1_get_transform, (Component_1* __this, MethodInfo* method));
DO_APP_FUNC(0x00FA8BA0, GameObject*, Component_1_get_gameObject, (Component_1* __this, MethodInfo* method));

/* these do not work because the __MethodInfo is always null
DO_APP_FUNC(0x01B11760, Collider__Array*, GameObject_GetComponentsInChildren_12, (GameObject* __this, bool includeInactive, MethodInfo* method));
DO_APP_FUNC_METHODINFO(0x04EF9470, GameObject_GetComponentsInChildren_12__MethodInfo);
DO_APP_FUNC(0x01B11690, BoxCollider*, GameObject_GetComponent_36, (GameObject* __this, MethodInfo* method));
DO_APP_FUNC_METHODINFO(0x04EFA190, GameObject_GetComponent_36__MethodInfo);
*/
DO_APP_FUNC(0x00C01930, Component_1__Array*, GameObject_GetComponentsInChildren_1, (GameObject* __this, Type* type, bool includeInactive, MethodInfo* method));

DO_APP_FUNC(0x010693D0, Vector3, Transform_get_position, (Transform* __this, MethodInfo* method));
DO_APP_FUNC(0x01066CA0, Transform*, Transform_get_root, (Transform* __this, MethodInfo* method));
DO_APP_FUNC(0x01067FC0, void, Transform_Rotate_2, (Transform* __this, float xAngle, float yAngle, float zAngle, Space__Enum relativeTo, MethodInfo* method));
DO_APP_FUNC(0x010672A0, void, Transform_LookAt_1, (Transform* __this, Transform* target, MethodInfo* method));
DO_APP_FUNC(0x010698B0, void, Transform_set_eulerAngles, (Transform* __this, Vector3 value, MethodInfo* method));
DO_APP_FUNC(0x01069A80, void, Transform_set_localEulerAngles, (Transform* __this, Vector3 value, MethodInfo* method));
DO_APP_FUNC(0x01068C40, int32_t, Transform_get_childCount, (Transform* __this, MethodInfo* method));
DO_APP_FUNC(0x01066AE0, Transform*, Transform_GetChild, (Transform* __this, int32_t index, MethodInfo* method));

DO_APP_FUNC(0x013212F0, Bounds, Collider_get_bounds, (Collider* __this, MethodInfo* method));

DO_APP_FUNC(0x01324C00, bool, Physics_Raycast_4, (Vector3 origin, Vector3 direction, RaycastHit* hitInfo, float maxDistance, int32_t layerMask, QueryTriggerInteraction__Enum queryTriggerInteraction, MethodInfo* method));
DO_APP_FUNC(0x01325BD0, bool, Physics_SphereCast, (Vector3 origin, float radius, Vector3 direction, RaycastHit* hitInfo, float maxDistance, int32_t layerMask, QueryTriggerInteraction__Enum queryTriggerInteraction, MethodInfo* method));

DO_APP_FUNC(0x0005B300, Collider*, RaycastHit_get_collider, (RaycastHit__Boxed* __this, MethodInfo* method));

DO_APP_FUNC(0x0101FC20, int32_t, Screen_get_width, (MethodInfo* method));
DO_APP_FUNC(0x0101FBF0, int32_t, Screen_get_height, (MethodInfo* method));


/* potential ToString() methods
DO_APP_FUNC(0x00F849C0, String *, Object_1_ToString, (Object_1 * __this, MethodInfo * method));
DO_APP_FUNC(0x00F84A30, String *, Object_1_ToString_1, (Object_1 * obj, MethodInfo * method));

DO_APP_FUNC(0x00F84AF0, String *, Object_1_get_name, (Object_1 * __this, MethodInfo * method));
DO_APP_FUNC(0x00F83C30, String *, Object_1_GetName, (Object_1 * obj, MethodInfo * method));


DO_APP_FUNC(0x005C4DB0, String *, SFSUser_ToString, (SFSUser * __this, MethodInfo * method));
*/