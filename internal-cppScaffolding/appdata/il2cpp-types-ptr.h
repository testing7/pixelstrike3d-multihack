// Generated C++ file by Il2CppInspector - http://www.djkaty.com - https://github.com/djkaty
// Target Unity version: 2020.1.11 - 2020.1.17

// ******************************************************************************
// * IL2CPP application-specific type definition addresses
// ******************************************************************************

DO_TYPEDEF(0x04F67388, Object);
DO_TYPEDEF(0x04F688E8, String);
DO_TYPEDEF(0x04F3E1A8, Component_1);
DO_TYPEDEF(0x04F58860, GameManager);
DO_TYPEDEF(0x04F2E370, Behaviour);
DO_TYPEDEF(0x04F368C0, Camera);
DO_TYPEDEF(0x04F54EB8, SmartFox);
DO_TYPEDEF(0x04F67848, SFSUser);
DO_TYPEDEF(0x04F51750, ft_ru0082XR__Enum);
DO_TYPEDEF(0x04F539A0, NetworkEntity);
DO_TYPEDEF(0x04F587D0, NetworkManager);
DO_TYPEDEF(0x04F4AC70, Dictionary_2_System_Int32_NetworkEntity_);
DO_TYPEDEF(0x04F66C38, Int32__Array);
DO_TYPEDEF(0x04F32930, GameObject);
DO_TYPEDEF(0x04F36CB0, Object_1);
DO_TYPEDEF(0x04F30AD0, MonoBehaviour);
DO_TYPEDEF(0x04F2D0E0, RaycastHit);
DO_TYPEDEF(0x04F33590, Collider);
DO_TYPEDEF(0x04F36950, Vector2);
DO_TYPEDEF(0x04F360E0, Matrix4x4);
DO_TYPEDEF(0x04F36AB8, Vector3);
DO_TYPEDEF(0x04F36758, Vector4);
DO_TYPEDEF(0x04F2BE50, Transform);
DO_TYPEDEF(0x04F29618, Space__Enum);
DO_TYPEDEF(0x04F33E90, Vector2Int);
DO_TYPEDEF(0x04F68390, Type);
DO_TYPEDEF(0x04F36170, Component_1__Array);
DO_TYPEDEF(0x04F36A70, Bounds);